﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FlasherScript : MonoBehaviour {

    
    public SpriteRenderer image;
    public Color color;
    public float time=2f;

	// Use this for initialization
	void Start () {
        StartCoroutine (Movement());

        
        
	}

    IEnumerator Movement()
    {

        while (enabled)
        {
           // yield return new WaitForSeconds(.5f);
            for (float t = 0; t < time; t += Time.deltaTime)
            {
                float frac = t / time;
                
                image.color = Color.Lerp(color, Color.black, frac);
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(.5f);

            for (float t = 0; t < time; t += Time.deltaTime)
            {
                float frac = t / time;
               
                image.color = Color.Lerp(Color.black, color, frac);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
