﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomText : MonoBehaviour {

    public TextMesh text;

    // Use this for initialization
    void Start () {
        text = GetComponentInChildren<TextMesh>();
	}

    private void OnEnable()
    {
       // if (GameManager.instance.objective > 2) { door1.SetActive(false); door1.SetActive(false); door2.SetActive(false); keycode.SetActive(false); }
        if (Random.value < 0.2f) { text.text = "A mind without purpose \nwill wander in dark places"; }
        else if (Random.value < 0.2f) { text.text = "Every lone spirit \ndoubts his strength"; }
        else if (Random.value < 0.2f) { text.text = "Excuses are the refuge \nof the weak"; }
        else if (Random.value < 0.2f) { text.text = "Know your duty!"; }
        else if (Random.value < 0.2f) { text.text = "Never forget, never forgive."; }
        else if (Random.value < 0.2f) { text.text = "You should be running"; }
        else if (Random.value < 0.2f) { text.text = "Only the Lost \nunderstand true terror"; }
        else if (Random.value < 0.2f) { text.text = "Reach out to embrace the \nglories that will come"; }
        else if (Random.value < 0.2f) { text.text = "Dark dreams lie \nupon the heart"; }
        else
        {
            text.text = "E SUN THE SUN THE SUN \nTHE SUN THE SUN THE SUN\nTHE SUN THE SUN TH";
        }
    }
}
