﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

    public Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider c)
    {
        //Debug.Log("DOOR");
        anim.SetTrigger("Open");
        AudioManager.instance.PlaySFX("DoorHiss");
    }

    private void OnTriggerExit(Collider c)
    {
        anim.SetTrigger("Close");
    }
}
