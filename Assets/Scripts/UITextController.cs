﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextController : MonoBehaviour {


    public static UITextController instance;

    public GameObject textBox;
    public GameObject promptBox;
    public Text signText;
    public Text promptText;

    public Button prevButton;
    public Button nextButton;
    public Text prevText;
    public Text nextText;

    public State state = State.Hidden;

    private string[] aText;
    public int currentMessage;

    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }

        prevText = prevButton.GetComponentInChildren<Text>();
        nextText = nextButton.GetComponentInChildren<Text>();
    }


    private void Update()
    {
        switch (state)
        {
            case State.Hidden:
                promptBox.SetActive(false);
                textBox.SetActive(false);
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                VisibleUpdate();
                break;
            case State.Done:
                DoneUpdate();
                break;
            default:
                break;

        }
    }


    public static void Show(string[] messages)
    {

        instance.aText = messages;

        instance.ShowPrompt();


    }

    public static void Hide()
    {
        instance.state = State.Hidden;

    }

    void ShowPrompt()
    {
        state = State.Prompting;
        currentMessage = 0;
        textBox.SetActive(false);
        promptBox.SetActive(true);
        promptText.text = "Read";

        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void PromptUpdate()
    {
        if (Input.GetButtonDown("E"))
        {
            GameManager.instance.inMenu = true;
            state = State.Visible;
            textBox.SetActive(true);
            promptText.text = "Next";
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            AudioManager.instance.PlaySFX("tone1");
            Time.timeScale = 0;
        }

    }

    void VisibleUpdate()
    {
        signText.text = aText[currentMessage];

        signText.text = signText.text.Replace("@", "\n");

        if (currentMessage == 0) { prevButton.interactable=false; prevText.color = Color.clear; } else { prevButton.interactable = true; prevText.color = Color.white; }
        if (currentMessage >= aText.Length - 1) { nextButton.interactable = false; nextText.color = Color.clear; } else { nextButton.interactable = true; nextText.color = Color.white; }

        

    }

    void DoneUpdate()
    {
        if (Input.GetButtonDown("E"))
        {
            ShowPrompt();
        }
    }

    IEnumerator DisplayMessages()
    {
        textBox.SetActive(true);
        for (int i = 0; i < aText.Length; i++)
        {
            promptBox.SetActive(false);
            bool isLast = (i == aText.Length - 1);
            yield return StartCoroutine(DisplayMessage(aText[i], isLast));
        }
    }

    IEnumerator DisplayMessage(string message, bool isLast)
    {
        string s = "";
        for (int i = 0; i < aText.Length; i++)
        {
            s += message[i];
            signText.text = s;
            if (Input.GetButton("Fire2")) yield return new WaitForEndOfFrame();
            else yield return new WaitForSecondsRealtime(.3f);
        }
        yield return new WaitUntil(() => !Input.GetButton("E"));
        promptBox.SetActive(true);
        if (isLast) promptText.text = "Done";
        else promptText.text = "Next";
        yield return new WaitUntil(() => Input.GetButton("E"));
    }


    public void PrevButton()
    {
        currentMessage--;
    }

    public void NextButton()
    {
        currentMessage++;

    }

    public void ExitButton()
    {
        ShowPrompt();
        AudioManager.instance.PlaySFX("tone1");
        GameManager.instance.inMenu = false;
    }

}
