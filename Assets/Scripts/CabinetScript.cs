﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabinetScript : MonoBehaviour {

    public Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("Player") )
        {

            anim.SetTrigger("Open");

        }
    }

  //  private void OnTriggerExit(Collider c)
  //  {
  //      if (c.CompareTag("Player"))
  //      {
  //
  //          anim.SetTrigger("Close");
  //
  //      }
  //  }
}
