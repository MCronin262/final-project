﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{

    public static SaveManager instance;
    public int fileID;

    [Serializable]
    public class SaveFile
    {
        public string name;
        public string scene;
        public Vector3 pos;
        public Vector3 rot;
        public float health;
        public float maxHealth;
        public float objective;
        public float parts;
        public bool pressurized=false;
        public int partA;
        public int partB;
        public int partC;
        public int partD;
        public float secretParts;
    }

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public void Create(int fileID, string name)
    {
        SaveFile saveFile = new SaveFile();
        //saveFile.scene = "Main";
        saveFile.name = name;
        
        saveFile.pos = new Vector3(50, 0, 43);
        saveFile.rot = new Vector3 (0,0,0);

        saveFile.objective = 0;
        saveFile.parts = 4;
        saveFile.pressurized = false;

        string filename = "SaveFile" + fileID + ".json";
        string path = Path.Combine(Application.persistentDataPath, filename);
        string json = JsonUtility.ToJson(saveFile);

        File.WriteAllText(path, json);
    }

    public void Save()
    {
        SaveFile saveFile = Read(instance.fileID);
        saveFile.scene = SceneManager.GetActiveScene().name;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        saveFile.pos = player.transform.position;
        saveFile.rot = camera.transform.localEulerAngles;

        saveFile.objective = GameManager.instance.objective;
        saveFile.parts = GameManager.instance.parts;
        saveFile.secretParts = GameManager.instance.secretParts;

        saveFile.partA = GameManager.instance.partA;
        saveFile.partB = GameManager.instance.partB;
        saveFile.partC = GameManager.instance.partC;
        saveFile.partD = GameManager.instance.partD;


        saveFile.pressurized = GameManager.instance.pressurized;

        string filename = "SaveFile" + instance.fileID + ".json";
        string path = Path.Combine(Application.persistentDataPath, filename);
        string json = JsonUtility.ToJson(saveFile);

        File.WriteAllText(path, json);
        Debug.Log(path);
    }

    public SaveFile Read(int fileID)
    {
        string filename = "SaveFile" + fileID + ".json";
        string path = Path.Combine(Application.persistentDataPath, filename);
        if (!File.Exists(path)) return null;

        string json = File.ReadAllText(path);
        return JsonUtility.FromJson<SaveFile>(json);
    }

    public void Load(int fileID)
    {
        instance.fileID = fileID;
        SaveFile saveFile = Read(fileID);
        instance.StartCoroutine(instance.LoadFile(saveFile));
    }

    IEnumerator LoadFile(SaveFile saveFile)
    {
        yield return SceneManager.LoadSceneAsync(saveFile.scene);
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        player.transform.position = saveFile.pos;
        camera.transform.localEulerAngles = saveFile.rot;

        GameManager.instance.objective = saveFile.objective;
        GameManager.instance.parts = saveFile.parts;
        GameManager.instance.pressurized = saveFile.pressurized ;

        GameManager.instance.secretParts = saveFile.secretParts;

         GameManager.instance.partA=saveFile.partA ;
         GameManager.instance.partB=saveFile.partB ;
         GameManager.instance.partC=saveFile.partC ;
         GameManager.instance.partD= saveFile.partD ;


        //health.health = saveFile.health;
        //health.maxHealth = saveFile.maxHealth;	
    }

    public void Delete(int fileID)
    {
        string filename = "SaveFile" + fileID + ".json";
        string path = Path.Combine(Application.persistentDataPath, filename);
        if (File.Exists(path)) File.Delete(path);
    }


}
