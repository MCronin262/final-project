﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeKeypadController : MonoBehaviour {

    // public Animator anim;
    public bool inthething=false;
    public static BridgeKeypadController instance;

    public GameObject door1;
    public GameObject door2;
    public GameObject keycode;
     Animator anim1;
     Animator anim2;
    public BoxCollider padTrigger;
    public GameObject introCam;
    Animator introAnim;
    public GameObject player;
    public GameObject playerCam;
    //public TextMesh text;
    public GameObject goButton;
    public GameObject partGlow;


    private void Awake()
    {
        if (!instance) { instance = this; }
       
    }


    void Start()
    {
        
        anim1 = door1.GetComponent<Animator>();
        anim2 = door2.GetComponent<Animator>();
        if (GameManager.instance.objective == -1) { StartCoroutine(NG()); }
        else if (GameManager.instance.objective >= 0 && GameManager.instance.objective < 2) { player.transform.position = new Vector3(3, .85f, -2); }
        else if (GameManager.instance.objective > 2)
        {
            door1.SetActive(false);
            door1.SetActive(false);
            door2.SetActive(false);
            keycode.SetActive(false);
            padTrigger.enabled = false;
        }
         if (GameManager.instance.objective < 9) { goButton.SetActive(false); }
         if (GameManager.instance.objective < 7) { partGlow.SetActive(false); }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void AirlockOpen()
    {
    //    anim.SetTrigger("Open");
    }

    

    private void OnEnable()
    {
      

    }



    private void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("Player") && BridgeCanvasController.instance.used == false)
        {

            BridgeCanvasController.instance.ShowPrompt();
            inthething = true;
        }
    }


    private void OnTriggerExit(Collider c)
    {
        if (c.CompareTag("Player"))
        {
            BridgeCanvasController.Hide();
            
            inthething = false;
        }

    }

    public void OpenDoors()
    {
        AudioManager.instance.PlaySFX("zap1");
        anim1.SetTrigger("Open");
        anim2.SetTrigger("Open");
        AudioManager.instance.PlaySFX("Steam");
        StartCoroutine(Feep());
        //GameManager.instance.objective = 4;
    }

    

    IEnumerator Feep()
    {
        yield return new WaitForSeconds(3);
        padTrigger.enabled = false;
        yield break;
        
    }

    IEnumerator NG()
    {
        GameManager.instance.inMenu = true;
        player.SetActive(false);
        player.transform.position = new Vector3(3, .85f, -2);
        NewGame();
        yield return new WaitForSecondsRealtime(5.2f);

        player.SetActive(true);
        yield return new WaitForSecondsRealtime(.3f);
        
        introCam.SetActive(false);
        GameManager.instance.inMenu = false;
        // yield return new WaitForSecondsRealtime(.6f);


        GameManager.instance.objective = 0;
        yield break;

    }

    //yield return new WaitForSecondsRealtime(.6f);
    public void NewGame() { introCam.SetActive(true); }
    }
