﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupController : MonoBehaviour {

    public static PickupController instance;

    public int ID = 0;
    

	// Use this for initialization
	void Start () {
		if (ID == 2 && GameManager.instance.objective>=2) { gameObject.SetActive(false); }
        if (ID == 3 && GameManager.instance.objective >= 5) { gameObject.SetActive(false); }
        


        if (!instance) { instance = this; }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("Player"))
        {
            UseCanvasController.instance.ShowPrompt();
            UseCanvasController.instance.ID = ID;
        }
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.CompareTag("Player"))
        {
            UseCanvasController.instance.Hide();
            UseCanvasController.instance.ID = 0;
        }
    }

    public void Vanish()
    {
        gameObject.SetActive(false);
    }

    public void Install()
    {
        gameObject.SetActive(false);
        
    }
}
