﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartController : MonoBehaviour {

    public int ID = 0;
    BoxCollider box;
    public GameObject twin;

    private void Awake()
    {
        box = GetComponent<BoxCollider>();
    }

    // Use this for initialization
    void Start () {
        if (GameManager.instance.objective < 7&&ID!=20) { box.enabled = false; }
        

            if (ID == 4 && GameManager.instance.partA > 0) { gameObject.SetActive(false); }
            else if (ID == 5 && GameManager.instance.partB > 0) { gameObject.SetActive(false); }
            else if (ID == 6 && GameManager.instance.partC > 0) { gameObject.SetActive(false); }
            else if (ID == 7 && GameManager.instance.partD > 0) { gameObject.SetActive(false); }
            else if ((ID == 8 && GameManager.instance.partA > 1)  || (ID == 8 && GameManager.instance.objective  > 8)|| (ID == 8 && GameManager.instance.objective  <6)) { gameObject.SetActive(false); }
            else if ((ID == 9 && GameManager.instance.partB > 1)  || (ID == 9 && GameManager.instance.objective  > 8)|| (ID == 9 && GameManager.instance.objective  <6)) { gameObject.SetActive(false); }
            else if ((ID == 10 && GameManager.instance.partC > 1) || (ID == 10 && GameManager.instance.objective > 8)|| (ID == 10 && GameManager.instance.objective <6)) { gameObject.SetActive(false); }
            else if ((ID == 11 && GameManager.instance.partD > 1) || (ID == 11 && GameManager.instance.objective > 8) || (ID == 11 && GameManager.instance.objective <6)) { gameObject.SetActive(false); }
            else if (ID == 12 && GameManager.instance.partA <2) { gameObject.SetActive(false); }
            else if (ID == 13 && GameManager.instance.partB <2) { gameObject.SetActive(false); }
            else if (ID == 14 && GameManager.instance.partC <2) { gameObject.SetActive(false); }
            else if (ID == 15 && GameManager.instance.partD <2) { gameObject.SetActive(false); }

    }

   
	
	

    private void OnTriggerEnter(Collider c)
    {
        ShowPrompt();
       // Debug.Log("Huh");
        if (c.CompareTag("Player"))
        {
            ShowPrompt();
          //  Debug.Log("Huh");
        }
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.CompareTag("Player"))
        {
            Hide();
            
        }
    }

    public void Vanish()
    {
        gameObject.SetActive(false);

        if (GameManager.instance.parts == 1) { GameManager.instance.parts -= 1; GameManager.instance.objective = 8; }
        else { GameManager.instance.parts -= 1; }
    }

    public void Install()
    {
        gameObject.SetActive(false);
        if (!twin) { }
        else { twin.gameObject.SetActive(true); }
        AudioManager.instance.PlaySFX("zap1");
        if (GameManager.instance.secretParts == 1) { GameManager.instance.secretParts -= 1; GameManager.instance.objective = 9; }
        else { GameManager.instance.secretParts -= 1; }

    }


    
    public State state = State.Hidden;


    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }


   


    private void Update()
    {
        switch (state)
        {
            case State.Hidden:
              //  GameManager.instance.partPopup.SetActive(false);
                
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                VisibleUpdate();
                break;
            case State.Done:
                DoneUpdate();
                break;
            default:
                break;

        }

        if (ID==8&&GameManager.instance.objective > 7) { box.enabled = true; }
    }



    public void Hide()
    {
        state = State.Hidden;
        GameManager.instance.partPopup.SetActive(false);
    }

    public void ShowPrompt()
    {
        state = State.Prompting;

        GameManager.instance.partPopup.SetActive(true);
        if (ID > 3 && ID < 8) { GameManager.instance.partText.text = "Pick up"; }
        else if (ID > 7 && ID < 12) { GameManager.instance.partText.text = "Install"; }
        else  if (ID ==20) { GameManager.instance.partText.text = "Pick up"; }
        else { GameManager.instance.partText.text = "Use"; }

    }

    void PromptUpdate()
    {
        if (Input.GetButtonDown("E"))
        {
           
            if (ID == 4)
            {
                GameManager.instance.partA = 1;
                Vanish();
                Hide();
            }
            else if (ID == 5)
            {
                GameManager.instance.partB = 1;
                Vanish();
                Hide();
            }
            else if (ID == 6)
            {
                GameManager.instance.partC = 1;
                Vanish();
                Hide();
            }
            else if (ID == 7)
            {
                GameManager.instance.partD = 1;
                Vanish();
                Hide();
            }
            else if (ID == 8 && GameManager.instance.partA == 1)
            {
                GameManager.instance.partA = 2;
                Install();
                Hide();
              //  GameManager.instance.parts -= 1;
            }
            else if (ID == 9&&GameManager.instance.partB==1)
            {
                GameManager.instance.partB = 2;
                Install();
                Hide();
              
            }
            else if (ID == 10 && GameManager.instance.partC == 1)
            {
                GameManager.instance.partC = 2;
                Install();
                Hide();
              
            }
            else if (ID == 11 && GameManager.instance.partD == 1)
            {
                GameManager.instance.partD = 2;
                Install();
                Hide();
               
            }
            else if (ID == 20)
            {
                gameObject.SetActive(false);
                Hide();

            }
        }

    }

    void VisibleUpdate()
    {
        

    }

    void DoneUpdate()
    {



    }
}
