﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpookController : MonoBehaviour {

    
    public BoxCollider box;
    
    public bool haunt=false;
    public GameObject ghost;
    public ParticleSystem wind;

    // Use this for initialization
    void Start () {
        ghost.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider c)
    {
        if (!GameManager.instance.haunt ) { ghost.SetActive(true); box.enabled = false; Debug.Log("On"); GameManager.instance.haunt = true; }
        else if(GameManager.instance.haunt)
        {
            ghost.SetActive(false);
            box.enabled = false;
            Debug.Log("Off");
            wind.Play();

            if (Random.value < 0.5f)
            { AudioManager.instance.PlaySFX("GhostGo"); }
            else { AudioManager.instance.PlaySFX("GhostBreath"); }
            
        }
        else { }
    }
}
