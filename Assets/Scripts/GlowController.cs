﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowController : MonoBehaviour {

    public GameObject glow;

	// Use this for initialization
	void Start () {
		if (GameManager.instance.objective < 7) { glow.SetActive(false); }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
