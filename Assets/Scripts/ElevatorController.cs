﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElevatorController : MonoBehaviour {

    public static ElevatorController instance;

  //  public GameObject textBox;
    public GameObject promptBox;
  //  public Text signText;
    public Text promptText;
    public GameObject elevatorButtons;
    public bool buttonsShow=false;


    public State state = State.Hidden;

   // private string[] texts;
  //  private int currentMessage;

    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
    }


    private void Update()
    {
        switch (state)
        {
            case State.Hidden:
                promptBox.SetActive(false);
            //    textBox.SetActive(false);
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                VisibleUpdate();
                break;
            case State.Done:
             //   DoneUpdate();
                break;
            default:
                break;

        }
    }


  // public static void Show(string[] messages)
  // {
  //
  //     instance.texts = messages;
  //
  //     instance.ShowPrompt();
  //     
  //
  // }

    public static void Hide()
    {
        instance.state = State.Hidden;
        
        
       
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

   public void ShowPrompt()
    {
        state = State.Prompting;
       // currentMessage = 0;
     //   textBox.SetActive(false);
        promptBox.SetActive(true);
        promptText.text = "Use";
        
    }

    void PromptUpdate()
    {
        if (Input.GetButtonDown("E"))
        {
            Time.timeScale = 0.01f;
            state = State.Visible;
           // textBox.SetActive(true);
           // promptText.text = "Next";
            ButtonPopup();
            buttonsShow = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            AudioManager.instance.PlaySFX("zap2");
            promptBox.SetActive(false);
            GameManager.instance.inMenu = true;
        }

    }

    void VisibleUpdate()
    {
       // signText.text = texts[currentMessage];

        promptText.text = "Exit";
        Cursor.visible = true;

       // if (Input.GetButtonDown("Jump"))
       // {
       //  
       //     if (buttonsShow == true)
       //     {
       //        // state = State.Done;
       //        // promptText.text = "Done";
       //         buttonsShow = false;
       //         ShowPrompt();
       //         Goaway();
       //         Time.timeScale = 1;
       //         Cursor.visible = false;
       //         Cursor.lockState = CursorLockMode.Locked;
       //         AudioManager.instance.PlaySFX("zap1");
       //     }
       // }

    }



    public void ButtonPopup()
    {
        elevatorButtons.SetActive(true);
    }

    public void Goaway()
    {
        elevatorButtons.SetActive(false);
    }

    public void ExitButton()
    {
        buttonsShow = false;
        ShowPrompt();
        Goaway();
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        AudioManager.instance.PlaySFX("zap1");
        GameManager.instance.inMenu = false;
    }

}
