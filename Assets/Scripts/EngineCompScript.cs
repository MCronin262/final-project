﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EngineCompScript : MonoBehaviour {

    public static EngineCompScript instance;

    public GameObject textBox;
    public GameObject objBox;
    public GameObject buttons;
    public GameObject promptBox;

    public GameObject partBox;
    public Text partText;
   
    public Text promptText;

    public Text loreText;
   // public Text hudText;
    public Text objText;

   

    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }

    public State state = State.Hidden;

    void Awake () {
		if (instance == null) { instance = this; }
	}

    private void Start()
    {
        
        
    }

  

    void Update()
    {
        switch (state)
        {
            case State.Hidden:
                promptBox.SetActive(false);
                textBox.SetActive(false);
                objBox.SetActive(false);
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                EngUpdate();
                break;
            case State.Done:
               // DoneUpdate();
                break;
            default:
                break;

        }
    }


    void PromptUpdate()
    {
        if (Input.GetButtonDown("E"))
        {
            if (GameManager.instance.objective <= 6) { GameManager.instance.objective = 7; AirlockController.instance.Shine(); }
            //else if (GameManager.instance.secretParts == 0) { }

            state = State.Visible;
            textBox.SetActive(true);
            promptText.text = "Next";
            GameManager.instance.inMenu = true;
            AudioManager.instance.PlaySFX("tone1");
            
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            textBox.SetActive(true);
            objBox.SetActive(true);
            buttons.SetActive(true);
            partBox.SetActive(true);
        }

    }

    public static void Hide()
    {
        instance.state = State.Hidden;

    }

    public void Show()
    {
        state = State.Prompting;
        
        textBox.SetActive(false);
        promptBox.SetActive(true);
        promptText.text = "Use";

        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }



    public void EngOpen()
    {
        
        //EngShowText();
        
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        textBox.SetActive (true);
        objBox.SetActive  (true);
        buttons.SetActive (true);

    }

    public void EngClose()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        textBox.SetActive(false);
        objBox.SetActive(false);
        buttons.SetActive(false);
        partBox.SetActive(false);

        state = State.Prompting;
        GameManager.instance.inMenu = false;

        promptBox.SetActive(true);
        promptText.text = "Use";

        Cursor.visible = false;
    }


    public void EngUpdate()
    {
        
        if (GameManager.instance.objective <=7)
        {
            objText.text = "AAAAAAH";
            loreText.text = "ENGINE'S BORKED, CHIEF. NICE PATCH JOB THOUGH. \n\n" +GameManager.instance.parts+ " PARTS NEEDED TO RESTORE MINIMAL FUNCTIONALITY. \n\n\n(What follows is a barely-comprehensible wall of technical jargon, equations, and graphs...)";
            partText.text = GameManager.instance.parts+"";
        }
        
        else if (GameManager.instance.objective >8)
        {
            objText.text = "GREEN LIGHT";
            loreText.text = "WE'RE BACK IN BUSINESS, CHEIF. \n\nREADY WHEN YOU ARE.";
            partText.text = GameManager.instance.parts + "";
        }

        else 
        {
            objText.text = "Objective: Bug bug";
            loreText.text = "You shouldn't be seeing this";
        }

       // if (Input.GetButtonDown("Jump"))
       // {
       //     EngClose();
       // }
    }

    


}
