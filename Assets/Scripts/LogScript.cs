﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogScript : MonoBehaviour {

    public static LogScript instance;

    public GameObject textBox;
   // public GameObject objBox;
    public GameObject buttons;
    public Button older;
    public Button newer;
    public GameObject promptBox;
    public Text promptText;

    public Text loreText;
    
   // public Text objText;

    float journalObj;
    bool fading = false;
    public bool jDelay = false;
    public bool buzzBuzz = false;

    private string[] aText;
    private int currentMessage;


    // Use this for initialization
    void Awake()
    {
        if (instance == null) { instance = this; }
    }

    private void Start()
    {
        // journalObj = GameManager.instance.objective;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("J"))
        {
            if (!textBox.gameObject.activeSelf && !jDelay)
            {
                JournalOpen();
                jDelay = true;
            }
            else
            {
                JournalClose();
                // jDelay = false;
            }
        }

        if (journalObj == 0) { older.interactable = false; } else { older.interactable = true; }
        if (journalObj == GameManager.instance.objective) { newer.interactable = false; } else { newer.interactable = true; }


        if (Input.GetButtonDown("E")&&buzzBuzz==true)
        {
           // state = State.Visible;
            textBox.SetActive(true);
           // promptText.text = "Next";
        }
    }

    public void JournalButtonOlder()
    {
        if (journalObj > 0 && !fading) { StartCoroutine(FadeOlder()); fading = true; }//  journalObj--; JournalShowText(); }
    }

    public void JournalButtonNewer()
    {
        if (journalObj < GameManager.instance.objective && !fading) { StartCoroutine(FadeNewer()); fading = true; }// journalObj++; JournalShowText(); }

    }

    public void JournalOpen()
    {
        journalObj = GameManager.instance.objective;
        JournalShowText();
        GameManager.instance.inMenu = true;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        textBox.SetActive(true);
       // objBox.SetActive(true);
        buttons.SetActive(true);
    }

    public void JournalClose()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        textBox.SetActive(false);
      //  objBox.SetActive(false);
        buttons.SetActive(false);
        jDelay = false;
        GameManager.instance.inMenu = false;
    }

    public static void Show(string[] messages)
    {

        instance.aText = messages;

        instance.ShowPrompt();

    }

    void ShowPrompt()
    {
       // state = State.Prompting;
        currentMessage = 0;
        textBox.SetActive(false);
        promptBox.SetActive(true);
        promptText.text = "Read";
        buzzBuzz = true;
    }

    public void JournalShowText()
    {

        loreText.text = aText[currentMessage];
    }

    IEnumerator FadeNewer()
    {

        for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
        {
            float frac = t / .5f;
           // objText.color = Color.Lerp(Color.white, Color.black, frac);
            loreText.color = Color.Lerp(Color.white, Color.black, frac);
            yield return new WaitForEndOfFrame();
        }
        journalObj++; JournalShowText();
        yield return new WaitForSecondsRealtime(.1f);


        for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
        {
            float frac = t / .5f;
           // objText.color = Color.Lerp(Color.black, Color.white, frac);
            loreText.color = Color.Lerp(Color.black, Color.white, frac);
            yield return new WaitForEndOfFrame();
        }
        fading = false;
        yield break;

    }

    IEnumerator FadeOlder()
    {

        // Debug.Log("Working");
        for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
        {
            float frac = t / .5f;
           // objText.color = Color.Lerp(Color.white, Color.black, frac);
            loreText.color = Color.Lerp(Color.white, Color.black, frac);
            yield return new WaitForEndOfFrame();
        }
        journalObj--; JournalShowText();
        yield return new WaitForSecondsRealtime(.1f);
        for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
        {
            float frac = t / .5f;
           // objText.color = Color.Lerp(Color.black, Color.white, frac);
            loreText.color = Color.Lerp(Color.black, Color.white, frac);
            yield return new WaitForEndOfFrame();
        }
        fading = false;
        yield break;

    }
    //
    // IEnumerator Delay()
    // {
    //     jDelay = true;
    //     yield return new WaitForSeconds(.5f);
    //     jDelay = false;
    //     yield break;
    // }


}
