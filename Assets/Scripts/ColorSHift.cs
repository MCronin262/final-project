﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSHift : MonoBehaviour {

    Material mt;

	// Use this for initialization
	void Start () {
        mt = GetComponent<Renderer>().material;
        StartCoroutine(EngineShift());
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    IEnumerator EngineShift()
    {
        while (enabled)
        {
            // Debug.Log("Working");
            for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                mt.color = Color.Lerp(Color.cyan, Color.white, frac);

                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSecondsRealtime(.1f);
            
            for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                mt.color = Color.Lerp(Color.white, Color.clear, frac);

                yield return new WaitForEndOfFrame();
            }

            for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                mt.color = Color.Lerp(Color.clear, Color.cyan, frac);

                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSecondsRealtime(.1f);
        }

    }
}
