﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public Vector3 velo;
    public static PlayerController instance;
    Animator anim;
    public Rigidbody rb;
    public float fowardSpeed = 10;
    public float turnSpeed = 5;
    public float maxSpeedChange = 1;
   // bool dead = false;
    public bool paused = false;
    public bool weakspot = false;

    public bool crouched = false;
    public bool jumping = false;

    public AudioSource footsteps;
    //public Camera camera;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        //  camera = GetComponentInChildren<Camera>();
       
    }




    private void Update()
    {
        //     float x = Input.GetAxis("Horizontal");
        //     float y = Input.GetAxis("Vertical");
        //
        //  transform.forward = camera.transform.forward * Time.deltaTime;
        //     // anim.SetFloat("s", x);
        //     // rb.velocity = transform.forward * y * fowardSpeed;
        //     // rb.angularVelocity = Vector3.up * x * turnSpeed;        
        //     Vector3 v = transform.forward * y * fowardSpeed;
        //     v.y = rb.velocity.y;
        //     Vector3 change = v - rb.velocity;
        //     if (change.magnitude > maxSpeedChange) change = change.normalized * maxSpeedChange;
        //     rb.AddForce(change, ForceMode.VelocityChange);
        //     //rb.velocity = v;        
        //     rb.angularVelocity = Vector3.up * x * turnSpeed;

        // anim.SetFloat("x", rb.angularVelocity.y);
        //anim.SetFloat("y", Vector3.Dot(rb.velocity, transform.forward));

      //  float z = Input.GetAxis("Vertical") * fowardSpeed;
      //  float x = Input.GetAxis("Horizontal") * fowardSpeed;
      //  z *= Time.deltaTime;
      //  x *= Time.deltaTime;
      //
      //  transform.Translate(x, 0, z);
      //
      //  velo = rb.velocity;
       

        if (Input.GetAxis("Vertical") >0 || Input.GetAxis("Horizontal") > 0) { anim.SetBool("Moving", true); } else { anim.SetBool("Moving", false); }
       
    }


        private void OnTriggerEnter(Collider c)
        {
            if (c.gameObject.tag == "BallReturn")
            {
                gameObject.transform.position = new Vector3(0, .2f, 0);
            }

            if (c.gameObject.tag == "EngTrigger" && GameManager.instance.objective < 4)
            {
                
                GameManager.instance.objective=4;
            }

        if (c.gameObject.tag == "GoButton" )
        {

            UseCanvasController.instance.ShowPrompt();
            UseCanvasController.instance.ID = 12;
        }


    }

    private void OnTriggerExit(Collider c)
        {
            

        }

       

    void SFX()
    {
       // if (Random.Range(1, 4) == 1) { AudioManager.instance.PlaySFX("footstep_metal_1"); }
       // if (Random.Range(1, 4) == 1) { AudioManager.instance.PlaySFX("footstep_metal_2"); }
       // else if (Random.Range(1, 4) == 1) { AudioManager.instance.PlaySFX("footstep_metal_3"); }
       // else { AudioManager.instance.PlaySFX("footstep_metal_4"); }

        if (Random.Range(1, 4) == 1) { AudioManager.instance.PlaySFX("footstep_wood_1"); }
        if (Random.Range(1, 4) == 1) { AudioManager.instance.PlaySFX("footstep_wood_2"); }
        else if (Random.Range(1, 4) == 1) { AudioManager.instance.PlaySFX("footstep_wood_3"); }
        else { AudioManager.instance.PlaySFX("footstep_wood_4"); }
    }


    
}



