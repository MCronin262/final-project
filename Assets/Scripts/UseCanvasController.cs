﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseCanvasController : MonoBehaviour {

    public static UseCanvasController instance;
    public int ID = 0;

    

   // public GameObject textBox;
    public GameObject promptBox;
   
    public Text promptText;
   
    public State state = State.Hidden;

    // private string[] texts;
    //  private int currentMessage;

    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
        
    }


    private void Update()
    {
        switch (state)
        {
            case State.Hidden:
                promptBox.SetActive(false);
          //      textBox.SetActive(false);
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                VisibleUpdate();
                break;
            case State.Done:
                   DoneUpdate();
                break;
            default:
                break;

        }
    }


    
    public  void Hide()
    {
        instance.state = State.Hidden;

    }

    public void ShowPrompt()
    {
        state = State.Prompting;
        
        promptBox.SetActive(true);
        if (ID > 3 && ID < 8) { promptText.text = "Pick up"; }
        else if (ID > 7 && ID < 12) { promptText.text = "Install"; }
        else { promptText.text = "Use"; }
        

    }

    void PromptUpdate()
    {
        if (Input.GetButtonDown("E"))
        {
           // Hide();
           if (ID == 1)
            {
                AirlockController.instance.Activate();
                //  Debug.Log("end");
                state = State.Visible;
            }
            if (ID == 2)
            {
                GameManager.instance.objective =2;
                PickupController.instance.Vanish();
                Hide();
            }
            if (ID == 3)
            {
                GameManager.instance.objective =5;
                PickupController.instance.Vanish();
                Hide();
            }
            if (ID == 4)
            {
                GameManager.instance.partA=1;
                PickupController.instance.Vanish();
                Hide();
            }
            if (ID == 5)
            {
                GameManager.instance.partB = 1;
                PickupController.instance.Vanish();
                Hide();
            }
            if (ID == 6)
            {
                GameManager.instance.partC = 1;
                PickupController.instance.Vanish();
                Hide();
            }
            if (ID == 7)
            {
                GameManager.instance.partD = 1;
                PickupController.instance.Vanish();
                Hide();
            }
            if (ID == 8)
            {
                GameManager.instance.partA = 2;
                PickupController.instance.Install();
                Hide();
            }
            if (ID == 9)
            {
                GameManager.instance.partB = 2;
                PickupController.instance.Install();
                Hide();
            }
            if (ID == 10)
            {
                GameManager.instance.partC = 2;
                PickupController.instance.Install();
                Hide();
            }
            if (ID == 11)
            {
                GameManager.instance.partD = 2;
                PickupController.instance.Install();
                Hide();
            }
            if (ID == 12)
            {
                GameManager.instance.EndTimes();
                Hide();
            }
        }

    }

    void VisibleUpdate()
    {
        if (ID == 1) {
            if (AirlockController.instance.working == true)
            {
                promptBox.SetActive(false);

            }
            else
            {
                promptBox.SetActive(true);
                state = State.Prompting;

            }
        }
        

    }

    void DoneUpdate()
    {

       

    }


}
