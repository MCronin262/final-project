﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AirlockController : MonoBehaviour {

    public static AirlockController instance;
   // public GameObject keycard;
    public Animator anim;
    public GameObject door1;
    public GameObject door2;
    //public bool pressurized=false;
    public bool working = false;
    public ParticleSystem noAir;
    public ParticleSystem moreAir;
    public TextMesh text;
    public GameObject doorText;

     Animator anim1;
     Animator anim2;

    public GameObject partA1;
    public GameObject partAGlow;
    public GameObject partB1;
    public GameObject partC1;
    public GameObject partD1;
    public BoxCollider partAA;

    // Use this for initialization
    void Awake () {
        anim = GetComponent<Animator>();
        anim1 = door1.GetComponent<Animator>();
        anim2 = door2.GetComponent<Animator>();
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        
       // if (Input.GetButtonDown("Fire1") && !working) { anim1.SetTrigger("Open"); }
    }

    private void Start()
    {
        if (GameManager.instance.objective <= 4) { doorText.SetActive(true); }

        if (GameManager.instance.objective >= 5 && !GameManager.instance.pressurized) { anim1.SetTrigger("Open"); }

        else if ( GameManager.instance.pressurized) { anim2.SetTrigger("Open"); text.text = "Depressurized"; }
       
    }
        
    
    public void Open()
    {
        if (GameManager.instance.objective <= 5) { GameManager.instance.objective = 6; }
        if (!GameManager.instance.pressurized) { anim2.SetTrigger("Open"); text.text = "Repressurize"; AudioManager.instance.PlaySFX("Steam"); }        
        else { anim1.SetTrigger("Open"); text.text = "Depressurized"; AudioManager.instance.PlaySFX("Steam"); }

    }

    public void Close()
    {
        if (!GameManager.instance.pressurized) { anim1.SetTrigger("Close"); text.text = "Working"; AudioManager.instance.PlaySFX("Steam"); }
        else { anim2.SetTrigger("Close"); text.text = "Working"; AudioManager.instance.PlaySFX("Steam"); }

    }

    public void AC()
    {
        if (!GameManager.instance.pressurized) { noAir.Play(); AudioManager.instance.PlaySFX("wizFlame"); }
        else { moreAir.Play(); AudioManager.instance.PlaySFX("wizFlame"); }

    }

    public void Flipper()
    {
        if (!GameManager.instance.pressurized) { GameManager.instance.pressurized = true; }
        else { GameManager.instance.pressurized = false; };
        working = false;
    }

    IEnumerator Depress()
    {

        yield break;
    }

    public void Activate()
    {

       // Debug.Log("Bug");
        if (!GameManager.instance.pressurized) { anim.SetTrigger("Depress"); working = true; }
        else { anim.SetTrigger("Repress"); working = true; }
    }

   public void Shine()
    {
        partA1.SetActive(true);
        partB1.SetActive(true);
        partC1.SetActive(true);
        partD1.SetActive(true);
        partAGlow.SetActive(true);
        partAA.enabled = true;
    }


    private void OnTriggerEnter(Collider c)
    {
        if (c.CompareTag("Player"))
        {
            UseCanvasController.instance.ShowPrompt();
            UseCanvasController.instance.ID=1;
        }
    }
  
    private void OnTriggerExit(Collider c)
    {
        if (c.CompareTag("Player"))
        {
            UseCanvasController.instance.Hide();
            UseCanvasController.instance.ID = 0;
        }
    }
}
