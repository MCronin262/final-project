﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BridgeCanvasController : MonoBehaviour {

    public static BridgeCanvasController instance;

    public GameObject promptBox;
    
    public Text promptText;
   
    public bool buttonsShow = false;
    public bool innit=false;
   
    public bool used = false;

    public State state = State.Hidden;

    public enum State
    {
        Hidden,
        Prompting,
        Visible,
        Done
    }


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
        
    }


    private void Update()
    {
        switch (state)
        {
            case State.Hidden:
                promptBox.SetActive(false);
          //      textBox.SetActive(false);
                break;
            case State.Prompting:
                PromptUpdate();
                break;
            case State.Visible:
                VisibleUpdate();
                break;
            case State.Done:
                   DoneUpdate();
                break;
            default:
                break;

        }
    }


    
    public static void Hide()
    {
        instance.state = State.Hidden;

    }

    public void ShowPrompt()
    {
        state = State.Prompting;
        // currentMessage = 0;
      //  textBox.SetActive(false);
        promptBox.SetActive(true);
        promptText.text = "Use";

    }

    void PromptUpdate()
    {
        if (Input.GetButtonDown("E")&&GameManager.instance.objective<=1)
        {
            GameManager.instance.objective = 1;
            state = State.Visible;
            
            // buttonsShow = true;
            AudioManager.instance.PlaySFX("zap2");
        }

        if (Input.GetButtonDown("E") && GameManager.instance.objective >1)
        {
            
            state = State.Done;
            BridgeKeypadController.instance.OpenDoors();
            used = true;
            GameManager.instance.unlocked = true;
            // padTrigger.enabled=false;

            promptText.text = "Access granted";
            GameManager.instance.objective = 3;
        }

    }

    void VisibleUpdate()
    {


        promptText.text = "Insufficient access level to lift lockdown";

        // Cursor.visible = true;
        if (Input.GetButtonDown("E"))
        { 
        state = State.Prompting;
        
        promptBox.SetActive(true);
        promptText.text = "Use";
            
        }

    }

    void DoneUpdate()
    {

       

    }


}
