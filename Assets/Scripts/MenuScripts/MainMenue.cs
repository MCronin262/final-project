﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenue : MonoBehaviour {
    
	public string nextLevel;
    public GameObject continueButton;
    
	void Start(){
		if (SaveFileExists()){
			SaveManager.SaveFile saveFile = SaveManager.instance.Read(0);
			continueButton.SetActive(true);
		}
		else{
			continueButton.SetActive(false);
		}
	}

	bool SaveFileExists(){
		int fileID = 0;
		return (SaveManager.instance.Read(fileID) != null);
	}

	public void PlayGame () {
        SaveManager.instance.Delete(0);
        SaveManager.instance.Create(0, "name");
		SceneManager.LoadScene (nextLevel);
        GameManager.instance.objective = -1;
    }

    public void Continue () {
        SaveManager.instance.Load(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
