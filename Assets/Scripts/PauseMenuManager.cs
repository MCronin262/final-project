﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour
{

    public RectTransform pauseMenuRoot;
    public Image fadeOut;

    void Awake()
    {
        ResumeGame();
    }

    public void PauseGame()
    {
        pauseMenuRoot.gameObject.SetActive(true);
        Time.timeScale = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ResumeGame()
    {
        pauseMenuRoot.gameObject.SetActive(false);
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SaveGame()
    {
        SaveManager.instance.Save();
    }

    public void MainMenu()
    {
        StartCoroutine(MainMenuFade());
    }

    void Update()
    {

        if (Input.GetButtonDown("Cancel")&&!GameManager.instance.inMenu)
        {
            //if (Scene.)
            //else 
            if (pauseMenuRoot.gameObject.activeSelf) ResumeGame();
            else PauseGame();

        }
    }


    IEnumerator MainMenuFade()
    {
        ResumeGame();
        fadeOut.enabled = true;
        GameManager.instance.inMenu = false;
        
        
        for (float t = 0; t < 1.5f; t += Time.unscaledDeltaTime)
        {
            float frac = t / 1.5f;
            fadeOut.color = Color.Lerp(Color.clear, Color.black, frac);

            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene("MainMenuScene");
        yield return new WaitForEndOfFrame();

    }

}
