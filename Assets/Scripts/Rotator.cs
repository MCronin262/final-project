﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (gameObject.tag == "Screensaver") { transform.Rotate(0, 0, -10 * Time.deltaTime); }
        else if (gameObject.tag == "SadCore") { transform.Rotate(0, -20 * Time.deltaTime, 0); }
        else if (gameObject.tag == "HappyCore") { transform.Rotate(0, -60 * Time.deltaTime, 0); }
        else if (gameObject.tag == "Debris") { transform.rotation = Random.rotation; }
        else if (gameObject.tag == "Ship") { transform.Rotate(0, -1 * Time.deltaTime, -2 * Time.deltaTime); }// -1 * Time.deltaTime); }
        else { transform.Rotate(0, 0, - 50 * Time.deltaTime); }
        
    }
}
