﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalScript : MonoBehaviour {

    public static JournalScript instance;

    public GameObject textBox;
    public GameObject objBox;
    public GameObject buttons;
    public Button older;
    public Button newer;
    public Text oldText;
    public Text newText;

    public Text loreText;
   // public Text hudText;
    public Text objText;

    float journalObj;
    bool fading = false;
    public bool jDelay = false;
     

    // Use this for initialization
    void Awake () {
		if (instance == null) { instance = this; }
	}

    private void Start()
    {
        // journalObj = GameManager.instance.objective;
        oldText = older.GetComponentInChildren<Text>();
        newText = newer.GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update () {
       // hudText.text = "Wishbone";
        if (Input.GetButtonDown("J"))
            {
            if (!textBox.gameObject.activeSelf && !jDelay)
            {
                JournalOpen();
                jDelay = true;
            }
            else
            {
                JournalClose();
               // jDelay = false;
            }
        }

        if (journalObj == 0) { older.interactable = false; oldText.color = Color.clear; } else { older.interactable = true; oldText.color = Color.white; }
        if (journalObj == GameManager.instance.objective) { newer.interactable = false; newText.color = Color.clear; } else { newer.interactable = true; newText.color = Color.white; }
    }

    public void JournalButtonOlder()
    {
        if (journalObj > 0 && !fading) { StartCoroutine(FadeOlder()); fading = true; }//  journalObj--; JournalShowText(); }
    }

    public void JournalButtonNewer()
    {
        if (journalObj < GameManager.instance.objective && !fading) { StartCoroutine(FadeNewer()); fading = true; }// journalObj++; JournalShowText(); }
        
    }

    public void JournalOpen()
    {
        journalObj = GameManager.instance.objective;
        JournalShowText();
        GameManager.instance.inMenu = true;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        textBox.SetActive (true);
        objBox.SetActive  (true);
        buttons.SetActive (true);
    }

    public void JournalClose()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        textBox.SetActive(false);
        objBox.SetActive(false);
        buttons.SetActive(false);
        jDelay = false;
        GameManager.instance.inMenu = false;
    }


    public void JournalShowText()
    {
        
        if (journalObj == 0)
        {
            objText.text = "Objective: Get to the bridge";
            loreText.text = "Nnngh. My head feels like it's been used as a power conduit... \n What happened? Why am I in the medbay? Where's Doctor Merill? Why are the lights off? Is that a DEAD BODY? \n What happened here? I need to get to the bridge... ";
        }
        else if (journalObj == -1)
        {
            objText.text = "Objective: Ow";
            loreText.text = "Ow...";
        }
        else if (journalObj == 1)
        {
            objText.text = "Objective: Find the Captain's access codes";
            loreText.text = "The bridge is locked. The bridge is locked, and the deck is deserted. Okay. Okay. This is the serious level of lockdown. I'll need the Captain's override code in order to lift it.";
        }
        else if (journalObj == 2)
        {
            objText.text = "Objective: Get to the bridge";
            loreText.text = "Got the codes. Back to the bridge. \n\nCaptains room was cleaned out. So whatever happened left at least some time to evac.";
        }
        else if (journalObj == 3)
        {
            objText.text = "Objective: Go to the Engine Room";
            loreText.text = "Okay. Bridge is intact, but main functions are all offline. \n\nLooks like Engineering is the next place to check. \n\n I have no idea what could be going on.";
        }
        else if (journalObj == 4)
        {
            objText.text = "Objective: Find an EnviroSuit on the crew deck";
            loreText.text = "Why is the hazard lock on? Where are all the EnviroSuits? I am asking a lot of questions today. \n\nHey, wasn't Saffy screwing around with a spare suit in their room?";
        }
        else if (journalObj == 5)
        {
            objText.text = "Objective: Return to the Engine Room";
            loreText.text = "Looks intact. Let's see why I needed this. \n\nThey had time to clean out our rooms, but then took the escape pods... What could make them cut and run like that?";
        }
        else if (journalObj ==6)
        {
            objText.text = "Objective: Investigate Engine room";
            loreText.text = "SAINT'S BREATH! \n\nWHAT THE HELL HAPPENED TO THE ENGINE ROOM??? \n\nWHAT COULD HAVE DONE THIS???";
        }
        else if (journalObj > 6 && journalObj <=7)
        {
            objText.text = "Objective: Repair Engine room: "+GameManager.instance.parts+" parts needed";
            loreText.text = "This is fine. This is fine. This is fiiiiiiine. \nOkay. Panic later. Survive now. \n\nIt looks like the guys actually managed to get a lot done before they abandoned ship. I should be able to limp somewhere safe with just a few more fixes. \n\nI'll need to search every deck to find the parts I need.";
        }
        else if (journalObj == 8)
        {
            objText.text = "Objective: Repair Engine room";
            loreText.text = "A thingy, a widget, a whatchamacallit, and a doohickey. \nAwesome. Back to the engine room. Time to stick things onto other things and plug whatsits into other whatsits.";
        }
        else if (journalObj == 9)
        {
            objText.text = "Objective: Return to the Bridge";
            loreText.text = "This... Actually looks like it could work. \nLet's go hit the button. \n\n Back to the bridge.";
        }
        else if (journalObj == 10)
        {
            objText.text = "Objective: Get the hell out of here";
            loreText.text = "YEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAW";
        }
        else 
        {
            objText.text = "Objective: Bug bug";
            loreText.text = "You shouldn't be seeing this";
        }
    }

    IEnumerator FadeNewer()
    {

            for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                objText.color = Color.Lerp(Color.white, Color.black, frac);
                loreText.color = Color.Lerp(Color.white, Color.black, frac);
                yield return new WaitForEndOfFrame();
            }
            journalObj++; JournalShowText();
            yield return new WaitForSecondsRealtime(.1f);
        

            for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                objText.color = Color.Lerp(Color.black, Color.white, frac);
                loreText.color = Color.Lerp(Color.black, Color.white, frac);
                yield return new WaitForEndOfFrame();
            }
        fading = false;
        yield break;
        
    }

    IEnumerator FadeOlder()
    {

       // Debug.Log("Working");
            for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                objText.color = Color.Lerp(Color.white, Color.black, frac);
                loreText.color = Color.Lerp(Color.white, Color.black, frac);
                yield return new WaitForEndOfFrame();
            }
            journalObj--; JournalShowText();
        yield return new WaitForSecondsRealtime(.1f);
        for (float t = 0; t < .5f; t += Time.unscaledDeltaTime)
            {
                float frac = t / .5f;
                objText.color = Color.Lerp(Color.black, Color.white, frac);
                loreText.color = Color.Lerp(Color.black, Color.white, frac);
                yield return new WaitForEndOfFrame();
            }
        fading = false;
        yield break;
        
    }
   //
   // IEnumerator Delay()
   // {
   //     jDelay = true;
   //     yield return new WaitForSeconds(.5f);
   //     jDelay = false;
   //     yield break;
   // }


}
