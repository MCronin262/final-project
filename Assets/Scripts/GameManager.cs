﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public bool unlocked = false;


    public float objective = 0;
    public float parts = 4;
    public float secretParts = 4;
    public int partA = 0;
    public int partB = 0;
    public int partC = 0;
    public int partD = 0;
    public bool pressurized = false;
    public GameObject partPopup;
    public Text partText;
    public Image fadeOut;
    public bool haunt;
    public bool inMenu=false;


    // Use this for initialization
    void Awake() {
        if (instance == null) { instance = this; DontDestroyOnLoad(this); }
        // Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;
    }

    // Update is called once per frame
    void Update() {
       // if (Input.GetButtonDown("Fire1")) { StartCoroutine(EndofEnds()); }
    }

    //  public void MainDeck() { SceneManager.LoadScene("MainDeck"); ElevatorController.instance.Goaway(); ElevatorController.Hide(); AudioManager.instance.PlaySFX("tone1"); }
    //  public void CargoDeck() { SceneManager.LoadScene("CargoDeck"); ElevatorController.instance.Goaway(); ElevatorController.Hide(); AudioManager.instance.PlaySFX("tone1"); }
    //  public void EngineDeck() { SceneManager.LoadScene("EngineDeck"); ElevatorController.instance.Goaway(); ElevatorController.Hide(); AudioManager.instance.PlaySFX("tone1"); }
     public void ExitElev() { ElevatorController.instance.ExitButton(); }

    public void MainDeck() { StartCoroutine(MainDeckFade()); }
    public void CargoDeck() { StartCoroutine(CargoFade()); }
    public void EngineDeck() { StartCoroutine(EngineFade()); }
    public void EndTimes() { StartCoroutine(EndofEnds()); }
    //public void ExitElev() { ElevatorController.instance.ExitButton(); }

    IEnumerator MainDeckFade()
    {
        GameManager.instance.inMenu = false;
        AudioManager.instance.PlaySFX("tone1");
        ElevatorController.instance.Goaway();
        ElevatorController.Hide();
        for (float t = 0; t < 1f; t += Time.deltaTime)
        {
            float frac = t / 1f;
            fadeOut.color = Color.Lerp(Color.clear, Color.black, frac);

            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene("MainDeck");
        yield return new WaitForEndOfFrame();

        for (float t = 0; t < .5f; t += Time.deltaTime)
        {
            float frac = t / .5f;
            fadeOut.color = Color.Lerp(Color.black, Color.clear, frac);

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator CargoFade()
    {
        GameManager.instance.inMenu = false;
        AudioManager.instance.PlaySFX("tone1");
        ElevatorController.instance.Goaway();
        ElevatorController.Hide();
        for (float t = 0; t < 1f; t += Time.deltaTime)
        {
            float frac = t / 1f;
            fadeOut.color = Color.Lerp(Color.clear, Color.black, frac);

            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene("CargoDeck");
        yield return new WaitForEndOfFrame();

        for (float t = 0; t < .5f; t += Time.deltaTime)
        {
            float frac = t / .5f;
            fadeOut.color = Color.Lerp(Color.black, Color.clear, frac);

            yield return new WaitForEndOfFrame();
        }
    }


    IEnumerator EngineFade()
    {
        GameManager.instance.inMenu = false;
        AudioManager.instance.PlaySFX("tone1");
        ElevatorController.instance.Goaway();
        ElevatorController.Hide();
        for (float t = 0; t < 1f; t += Time.deltaTime)
        {
            float frac = t / 1f;
            fadeOut.color = Color.Lerp(Color.clear, Color.black, frac);

            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene("EngineDeck");
        yield return new WaitForEndOfFrame();

        for (float t = 0; t < .5f; t += Time.deltaTime)
        {
            float frac = t / .5f;
            fadeOut.color = Color.Lerp(Color.black, Color.clear, frac);
       
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator EndofEnds()
    {


        for (float t = 0; t < 1f; t += Time.unscaledDeltaTime)
        {
            float frac = t / 1f;
            fadeOut.color = Color.Lerp(Color.clear, Color.black, frac);

            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene("EndScene");
        yield return new WaitForEndOfFrame();

        for (float t = 0; t < .2f; t += Time.deltaTime)
        {
            float frac = t / .2f;
            fadeOut.color = Color.Lerp(Color.black, Color.clear, frac);

            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(19);
        SceneManager.LoadScene("MainMenuScene");
    }

}
