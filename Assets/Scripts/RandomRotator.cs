﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour {

    float rand = 0;

	// Use this for initialization
	void Start () {
        rand = Random.Range(0, 7);
	}
	
	// Update is called once per frame
	void Update () {

        if (rand==0) { transform.Rotate(0, 0,-1 * Time.deltaTime); }
        else if (rand == 1) { transform.Rotate(0,  -1 * Time.deltaTime, -1 * Time.deltaTime); }
        else if (rand == 2) { transform.Rotate(0,  -1 * Time.deltaTime,0); }
        else if (rand == 3) { transform.Rotate(-1 * Time.deltaTime, -1 * Time.deltaTime, 0); }
        else if (rand == 4) { transform.Rotate(0, -1 * Time.deltaTime, 0); }
        else if (rand == 5) { transform.Rotate(0, -1 * Time.deltaTime, 0); }
        else if (rand == 6) { transform.Rotate(1 * Time.deltaTime, -1 * Time.deltaTime, -1 * Time.deltaTime); }
        else { transform.Rotate(0, 0, - 2 * Time.deltaTime); }
        
    }
}
